﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepositary<T> : IRepository<T>
        where T : BaseEntity
    {
        protected DataContext Data { get; set; }

        public EfRepositary(DataContext data)
        {
            Data = data;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await Data.Set<T>().ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entities = await Data.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            return entities;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            if (ids == null) return new List<T>();
            var entities = await Data.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await Data.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await Data.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetInclude(string propertyPath)
        {
            return await Data.Set<T>().Include(propertyPath).ToListAsync();

        }

        public async Task<T> GetIncludeByIds(string propertyPath, List<Guid> ids)
        {
            return await Data.Set<T>().Where(x => ids.Contains(x.Id)).Include(propertyPath).FirstOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        {
            await Data.Set<T>().AddAsync(entity);
            await Data.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await Data.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            Data.Set<T>().Remove(entity);
            await Data.SaveChangesAsync();
        }

    }

}
