﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            //_dataContext.AddRange(FakeDataFactory.Roles);
            //_dataContext.SaveChanges();

            //_dataContext.AddRange(FakeDataFactory.Preferences);
            //_dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Customers);

            _dataContext.AddRange(FakeDataFactory.Promocodes);
            _dataContext.SaveChanges();
        }

    }
}
