﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(p => p.CustomerId);

            modelBuilder
                .Entity<Preference>()
                .HasMany(p => p.Customers)
                .WithMany(c => c.Preferences)
                .UsingEntity(join => join.ToTable("PrefernceCustomer"));
            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasKey(bc => new { bc.PartnerId, bc.PreferenceId });
            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasOne(bc => bc.Partner)
            //    .WithMany(b => b.MainPromoCodePreferences)
            //    .HasForeignKey(bc => bc.PartnerId);
            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasOne(bc => bc.Preference)
            //    .WithMany()
            //    .HasForeignKey(bc => bc.PreferenceId);



            //modelBuilder.Entity<MainPromoCodePartnerPreference>().ToTable("MainPromoCodePartnerPreferences");

            //modelBuilder.Entity<PartnerPromoCodeLimit>().ToTable("PartnerPromoCodeLimits");
        }

    }
}
