﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }
        public ICollection<Customer> Customers { get; set; }
    }
}