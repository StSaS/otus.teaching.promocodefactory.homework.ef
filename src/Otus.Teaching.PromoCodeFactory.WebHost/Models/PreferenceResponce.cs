﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponce
    {
        public string Name { get; set; }

        public List<CustomerResponse> customerResponses { get; set; }
    }
}
