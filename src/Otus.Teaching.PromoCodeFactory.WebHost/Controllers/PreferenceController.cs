﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Get all Preferences
        /// </summary>
        /// <remarks>
        /// 
        /// 
        ///  Получение списка предпочтений
        ///     
        /// </remarks>
        [HttpGet]
        public async Task<List<PreferenceResponce>> GetPreferencesAsync()
        {
            IEnumerable<Preference> preferences = (IEnumerable<Preference>)await _preferenceRepository.GetAllAsync();
            List<PreferenceResponce> preferencesResponces = new List<PreferenceResponce>();

            foreach (Preference p in preferences)
            {
                preferencesResponces.Add(new PreferenceResponce()
                {
                    Name = p.Name,
                });
            }

            return preferencesResponces;
        }
    }
}
