﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;
        private IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Get all Customers
        /// </summary>
        /// <remarks>
        /// 
        /// 
        ///  Получение списка пользователей
        ///     
        /// </remarks>
        [HttpGet]
        public async Task<List<CustomerResponse>> GetCustomersAsync()
        {
            IEnumerable<Customer> customers = (IEnumerable<Customer>) await _customerRepository.GetInclude("Preferences");
            List<CustomerResponse> responses = new List<CustomerResponse>();
                
            foreach (Customer c in customers)
            {
                //IEnumerable<Preference> preferences1 = (IEnumerable<Preference>)_preferenceRepository.GetInclude(pref => true);
                //IEnumerable<Preference> preferences2 = (IEnumerable<Preference>) await _preferenceRepository.GetInclude("Customers");
                List<PreferenceResponce> preferences = new List<PreferenceResponce>();
                foreach (Preference preference in c.Preferences)
                {
                    preferences.Add(new PreferenceResponce()
                    {
                        Name = preference.Name,
                    });
                }

                responses.Add(new CustomerResponse()
                {
                    Id = c.Id,
                    LastName = c.LastName,
                    Email = c.Email,
                    FirstName = c.FirstName,
                    PreferenceResponces = preferences
                });
            }

            return responses;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            Customer customer = (Customer) await _customerRepository.GetByIdAsync(id);
            List<PromoCodeShortResponse> promocodes = new List<PromoCodeShortResponse>();
            List<PreferenceResponce> preferences = new List<PreferenceResponce>();

            foreach(PromoCode promo in customer.PromoCodes)
            {
                promocodes.Add(new PromoCodeShortResponse()
                {
                    Id = promo.Id,
                    PartnerName = promo.PartnerName,
                    BeginDate = promo.BeginDate.ToString(),
                    EndDate = promo.EndDate.ToString(),
                    Code = promo.Code,
                    ServiceInfo = promo.ServiceInfo
                });
            }

            foreach(Preference preference in customer.Preferences)
            {
                preferences.Add(new PreferenceResponce()
                {
                    Name = preference.Name,
                    customerResponses = (List<CustomerResponse>)preference.Customers
                });
            }

            CustomerResponse response = new CustomerResponse()
            {
                Id = customer.Id,
                LastName = customer.LastName,
                Email = customer.Email,
                FirstName = customer.FirstName,
                PromoCodes = promocodes,
                PreferenceResponces = preferences
            };
            
            return response;
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id}, customer.Id);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = (Customer ) await _customerRepository.GetIncludeByIds("Preferences", new List<Guid>() { id });

            if (customer == null)
                return NotFound();

            IEnumerable<Preference> preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return NoContent();

        }
    }
}